package com.weather.homework.api;

import java.io.Serializable;
import java.time.Instant;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "WeatherLog")
public class WeatherLog implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column
	private String responseId;
	
	@Column
	private String location;
	
	@Column
	private String actualWeather;
	
	@Column
	private java.sql.Timestamp timestamp;

	@Column
	private double temperature;
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getActualWeather() {
		return actualWeather;
	}

	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}
	
	@JsonProperty("weather")
	public void setWeather(List<Map<String, Object>> weatherEntries) {
		
		Map<String, Object> weather = weatherEntries.get(0);
		setId((Integer) weather.get("id"));
		setActualWeather((String) weather.get("description"));
	}	

	@JsonProperty("timestamp")
	public java.sql.Timestamp getTimestamp() {
		return this.timestamp;
	}

	public void setTimestamp(java.sql.Timestamp timestamp) {
		this.timestamp = timestamp;
	}	
	
	public double getTemperature() {
		return this.temperature;
	}

	public void setTemperature(double temperature) {
		this.temperature = temperature;
	}

	@JsonProperty("main")
	public void setMain(Map<String, Object> main) {
		setTemperature(Double.parseDouble(main.get("temp").toString()));
	}



}
