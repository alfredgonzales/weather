package com.weather.homework.api;

public class Weather extends WeatherLog {

	private String name;

	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
