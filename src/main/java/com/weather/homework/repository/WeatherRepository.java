package com.weather.homework.repository;

import com.weather.homework.api.Weather;
import com.weather.homework.api.WeatherLog;
import com.weather.homework.web.WeatherApi;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WeatherRepository extends JpaRepository<WeatherLog, Long>{
	
	/*
	boolean exists(WeatherLog weatherLog);
	void save(WeatherApi weatherApi);
	long count();
	*/
}
