package com.weather.homework.web;

import com.weather.homework.api.Weather;

public class WeatherApi {

	private final String location;

	private final String description;

	private final double temperature;

	WeatherApi( String location, Weather weather) {
		
		this.location = location;
		this.description = weather.getActualWeather();
		this.temperature = weather.getTemperature();
	}

	public String getLocation() {
		return this.location;
	}

	public String getDescription() {
		return this.description;
	}
	
	public double getTemperature() {
		return this.temperature;
	}

}
