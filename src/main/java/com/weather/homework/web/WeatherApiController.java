package com.weather.homework.web;

import java.util.ArrayList;
import java.util.List;


import com.weather.homework.WeatherApiApplicationProperties;
import com.weather.homework.api.Weather;
import com.weather.homework.api.WeatherService;
import com.weather.homework.repository.WeatherRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController    
@RequestMapping("/")
public class WeatherApiController {
	
	@Autowired
	WeatherRepository weatherRepository;

	private final WeatherService weatherService;

	private final WeatherApiApplicationProperties properties;

	public WeatherApiController(WeatherService weatherService, WeatherApiApplicationProperties properties) {
		this.weatherService = weatherService;
		this.properties = properties;
	}
	
	@RequestMapping(value ="getWeather")
	public List<WeatherApi> weatherSummary() {				
		List<WeatherApi> summary = (List<WeatherApi>) this.getSummary();
		/*if(weatherRepository.count() < 5) {
			for(WeatherApi weatherApi: summary) {
				if(!weatherRepository.exists(weatherApi)) {
					weatherRepository.save(weatherApi);
				}
			}
		}*/
		return summary;
	}	

	private Object getSummary() {
		List<WeatherApi> summary = new ArrayList<>();
		for (String location : this.properties.getLocations()) {
			
			Weather weather = this.weatherService.getWeather(location);
			summary.add(addWeatherApi(location, weather));
		}
		return summary;
	}

	private WeatherApi addWeatherApi( String location,
			Weather weather) {

		return new WeatherApi(location, weather);
	}

}

